The .service file starts procServ service named according to .service file, and automatically starts the IOC.

Check the .service file  refers to the correct location of the IOC start-up file.

Deploy the .servce file here:
    /etc/systemd/system/


Commands to see status of the service (exampe: "ioc_tscs_evr"):
- Status:	systemctl status ioc_tscs_evr
- Stop:		sudo systemctl stop ioc_tscs_evr
- Re-start:	sudo systemctl start ioc_tscs_evr
- Disable:	systemctl disable ioc_tscs_evr
- Enable:	systemctl enable ioc_tscs_evr


Telnet:
--------
To access the IOC:
telnet localhost 2000

To exit telnet:
Ctrl + ] , followed by "quit"

