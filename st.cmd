# e3-ioc start-up script for Target System Timing System - in standalone mode
#
####################

require mrfioc2,2.3.1-beta.5

####################
# Configuration
epicsEnvSet("P","Tgt-TWDS1000:Ctrl-EVR-01")

mrmEvrSetupPCI("EVR1",  "01:00.0")

# Not use in this script, but it is needed for the expansion (..not used?)
epicsEnvSet("MainEvtCODE", "14")

# Standard settings (..not used?)
#epicsEnvSet("HeartBeatEvtCODE", "122")
epicsEnvSet("ESSEvtClockRate", "88.0525")

# needed with software timestamp source w/o RT thread scheduling
#var evrMrmTimeNSOverflowThreshold 100000

####################
# Load Records
# Record names follows the template "$(P)$(R=)$(S=:)Signal-SD"
# "P" is mandatory, "R" is optional (default empty), "S" is optional (defaul separator ":")
# "DEV" is a unique identifier of the EVR card used by mrfioc2 device support layer
####################
dbLoadRecords("evr-pcie-300dc-univ.db", "P=$(P), EVR=EVR1, FEVT=$(ESSEvtClockRate)")


####################
iocInit()
####################

# Set delay compensation to 70 ns - bugfix for EVR loosing the timestamp for 5 seconds every 7-8 hours
dbpf $(P):DC-Tgt-SP 70

# Enable the EVR
dbpf $(P):Ena-Sel 1

####################
# Standalone Mode
####################
# Get current time from system clock
dbpf $(P):TimeSrc-Sel "Sys. Clock"

# Set up Prescaler 0 that will trigger sequencer at 14 Hz
dbpf $(P):PS-0-Div-SP 6289464

# Configure events (not needed for standalone?)
#dbpf $(P):EvtE-SP 14
#dbpf $(P):EvtE-SP.OUT "@OBJ=$(DEV0),Code=14"

# Configure sequencer
dbpf $(P):SoftSeq-0-RunMode-Sel "Normal"
dbpf $(P):SoftSeq-0-TrigSrc-2-Sel "Prescaler 0"
dbpf $(P):SoftSeq-0-TsResolution-Sel "uSec"
dbpf $(P):SoftSeq-0-Load-Cmd 1
dbpf $(P):SoftSeq-0-Enable-Cmd 1
dbpf $(P):SoftSeq-0-EvtCode-SP 2 14 127
dbpf $(P):SoftSeq-0-Timestamp-SP 2 0 1
dbpf $(P):SoftSeq-0-Commit-Cmd 1

# Configure delay generator (for output pulse)
dbpf $(P):DlyGen-0-Evt-Trig0-SP 14
dbpf $(P):DlyGen-0-Width-SP 1000

####################
# Configure UNIV outputs
####################
dbpf $(P):Out-RB02-Src-SP 0
dbpf $(P):Out-RB03-Src-SP 0
